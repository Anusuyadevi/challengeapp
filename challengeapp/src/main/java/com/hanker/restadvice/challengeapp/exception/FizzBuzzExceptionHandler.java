package com.hanker.restadvice.challengeapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.hanker.restadvice.challengeapp.dto.*;

@RestControllerAdvice

public class FizzBuzzExceptionHandler extends ResponseEntityExceptionHandler {

//TODO:: implement handler methods for FizzException, BuzzException and FizzBuzzException
	
	@ExceptionHandler(value = BuzzException.class)
	public ResponseEntity<FizzBuzzResponse> handleBuzzExceptiom(BuzzException ex) {
		FizzBuzzResponse responsemsg = new FizzBuzzResponse();
		responsemsg.setStatusCode(400);
		responsemsg.setMessage(ex.getMessage());

		return new ResponseEntity<FizzBuzzResponse>(responsemsg, HttpStatus.METHOD_NOT_ALLOWED);
	}
	@ExceptionHandler(value = FizzException.class)
	public ResponseEntity<FizzBuzzResponse> handleFizzExceptiom(FizzException ex) {
		FizzBuzzResponse responsemsg = new FizzBuzzResponse();
		responsemsg.setStatusCode(500);
		responsemsg.setMessage(ex.getMessage());

		return new ResponseEntity<FizzBuzzResponse>(responsemsg, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value = FizzBuzzException.class)
	public ResponseEntity<FizzBuzzResponse> handleFizzBuzzExceptiom(FizzBuzzException ex) {
		FizzBuzzResponse responsemsg = new FizzBuzzResponse();
		responsemsg.setStatusCode(507);
		responsemsg.setMessage(ex.getMessage());

		return new ResponseEntity<FizzBuzzResponse>(responsemsg, HttpStatus.INSUFFICIENT_STORAGE);
	}

	/*
	 * @ExceptionHandler(FizzException.class) public final ResponseEntity<Object>
	 * handleFizzException(Exception ex, WebRequest request) { FizzBuzzResponse eR =
	 * new
	 * FizzBuzzResponse(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR.value()));
	 * return new ResponseEntity<>(eR, HttpStatus.INTERNAL_SERVER_ERROR); }
	 * 
	 * @ExceptionHandler(BuzzException.class) public final ResponseEntity<Object>
	 * handleBuzzException(Exception ex, WebRequest request) { FizzBuzzResponse eR =
	 * new FizzBuzzResponse(ex.getMessage(), request.getDescription(false),
	 * ex.getClass()); return new ResponseEntity<>(eR, HttpStatus.BAD_REQUEST); }
	 * 
	 * @ExceptionHandler(FizzBuzzException.class) public final
	 * ResponseEntity<Object> handleFizzBuzzException(Exception ex) {
	 * FizzBuzzResponse eR = new FizzBuzzResponse(ex.getMessage(),
	 * HttpStatus.INSUFFICIENT_STORAGE.value()); return new ResponseEntity<>(eR,
	 * HttpStatus.INSUFFICIENT_STORAGE); }
	 */

}