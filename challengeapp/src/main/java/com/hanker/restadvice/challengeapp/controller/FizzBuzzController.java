package com.hanker.restadvice.challengeapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;
import com.hanker.restadvice.challengeapp.dto.*;
import com.hanker.restadvice.challengeapp.enums.FizzBuzzEnum;

@RestController

public class FizzBuzzController {

	@RequestMapping(value = "/controller_advice/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<ResponseMsg> getFizzBuzzResponse(@PathVariable("code") String code)

			throws FizzException, BuzzException, FizzBuzzException {

		if (FizzBuzzEnum.FIZZBUZZ.getValue().equals(code)) {
			
			throw new FizzBuzzException("In sufficient Storage",HttpStatus.INSUFFICIENT_STORAGE.toString());
			
			
		}else if (FizzBuzzEnum.FIZZ.getValue().equals(code)) {
			
			throw new FizzException("Internal Server Error",HttpStatus.INTERNAL_SERVER_ERROR.toString());
			

		} else if (FizzBuzzEnum.BUZZ.getValue().equals(code)) {
			throw new BuzzException("Method Not Allowed",HttpStatus.METHOD_NOT_ALLOWED.toString());
			
			/*
			 * return new ResponseEntity<FizzBuzzResponse>( new
			 * FizzBuzzResponse("Method Not Available",
			 * HttpStatus.METHOD_NOT_ALLOWED.value()), HttpStatus.OK);
			 * 
			 */


		}else {

		return new ResponseEntity<ResponseMsg>(new ResponseMsg("Success",HttpStatus.OK.value()),HttpStatus.OK);

		}

}
}